package no.gj.hg;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import no.gj.hg.systems.*;

public class Entity {
    /* Data from the map editor is contained here */
    private MapProperties props = null;

    Entity(MapProperties props) {
        this.props = props;
    }

    /* Performance critical data */
    public Body body = null;

    public Texture img = null;
    public RenderingCSystem.RenderData renderData;
    public HitSystem.HitData hitData;
    public ItemSystem.ItemHolderData itemHolderData;
    public ItemSystem.ItemData itemData;
    public InputCSystem.InputData inputData;
    public ScoringSystem.ScoringData scoringData;
    public Float width;
    public Float height;
    public Vector2 center = null;
    public Polygon polygon = null;
    public String name = "";
    public String objectName = "";
    public String saying = "";

    //is born shows whether the player is in game
    public boolean isBorn = true;
    public boolean isVisible = true;
    public boolean isGunActive = false;
    //public Vector2 direction = new Vector2(1,1);
    public float angleChange = 0f;

    public <T> T get(String prop, Class<T> c) {
        return props.get(prop, c);
    }
    public boolean has(String prop) {
        return props.containsKey(prop);
    }
    public void set(String prop, Object value) {
        props.put(prop, value);
    }
    public boolean propIs(String prop, String value) {
        return props.containsKey(prop) && props.get(prop, String.class).equals(value);
    }

    public void remove(String it) {
        props.remove(it);
    }
}

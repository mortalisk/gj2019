package no.gj.hg;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import no.gj.hg.systems.*;

import java.util.ArrayList;
import java.util.List;

public class Main extends ApplicationAdapter {

	GameState game;

	TiledMap tiledMap;
	@Override
	public void create () {
		game = new GameState();

		loadLevel("level1.tmx");
		game.tileWidth = tiledMap.getProperties().get("tilewidth", Integer.class);

		// note, order is very important.
		HitSystem hitSystem = new HitSystem();
		RenderingCSystem renderSystem = new RenderingCSystem(game);
		ItemSystem itemSystem = new ItemSystem();
		ScoringSystem scoringSystem = new ScoringSystem();
		game.systems.add(new CameraCSystem(game));
		game.systems.add(new TileMapCSystem(tiledMap));
		game.systems.add(renderSystem);
		InputCSystem inputSys =new InputCSystem();
		game.systems.add(inputSys);
		game.systems.add(new Physics(game));
		game.systems.add(new LightSystem(game));
		//game.systems.add(new GunSystem(game));
		game.systems.add(new TextboxCSystem());
		game.systems.add(hitSystem);
		game.systems.add(itemSystem);
		game.systems.add(scoringSystem);

		game.eventSystems.add(new SoundSystem());
		game.eventSystems.add(hitSystem);
		game.eventSystems.add(renderSystem);
		game.eventSystems.add(itemSystem);
		game.eventSystems.add(scoringSystem);
		game.eventSystems.add(inputSys);

		for (CSystem s: game.systems) {
			for (Entity e: game.entityList) {
				s.addEntity(e);
			}
		}

		game.addEvent(SoundSystem.loop("music"));

	}

	void loadLevel(String fileName) {
		tiledMap = new TmxMapLoader().load(fileName);

		Integer tileWidth = tiledMap.getProperties().get("tilewidth", Integer.class);

		for (MapLayer l : tiledMap.getLayers()) {
			if (l.isVisible())
				createEntities(tileWidth, l.getObjects());
		}
	}

	private void createEntities(Integer tileWidth, MapObjects objects) {
		for (MapObject obj : objects) {
			Entity e = EntityFactory.createEntity(obj, tileWidth);
			if (e.isBorn) {
				game.entityList.add(e);
				//adding only visible born
			}
			else{
				game.unbornBabies.add(e);
			}
		}
	}


	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		for (CSystem s : game.systems) {
			s.update(game);
		}
		List<Event> events = new ArrayList<Event>(game.events);
		game.events.clear();
		for (Event e: events) {
			for (EventSystem s: game.eventSystems) {
				s.run(e, game);
			}
		}


		for (Entity e : game.toAdd){
			long t1 = System.currentTimeMillis();
			game.entityList.add(e);
			for (CSystem s : game.systems) {
				s.addEntity(e);
			}
			long dt = System.currentTimeMillis() - t1;
			System.out.println("Adding object time: " + dt);
		}

		for (Entity e : game.toRemove) {
			game.entityList.remove(e);
			for (CSystem s : game.systems) {
				s.removeEntity(e);
			}
		}

		//clean up lists
		game.toAdd.clear();
		game.toRemove.clear();

	}
	
	@Override
	public void dispose () {
		for (CSystem s: game.systems) {
			s.dispose();
		}
	}
}


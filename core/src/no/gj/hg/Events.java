package no.gj.hg;

import com.badlogic.gdx.math.Vector2;
import no.gj.hg.systems.ScoringSystem;

public class Events {

    public static class EnterSensorEvent implements Event {
        public Entity senser;
        public Entity entered;
        public EnterSensorEvent(Entity senser, Entity entered) {
            this.senser = senser;
            this.entered = entered;
        }
    }

    public static class ExitSensorEvent implements Event {
        public Entity senser;
        public Entity exited;
        public ExitSensorEvent(Entity senser, Entity exited) {
            this.senser = senser;
            this.exited = exited;
        }
    }

    public static class EntityWasHitEvent implements Event {
        public Entity hitter;
        public Entity victim;
        public int comboCountOfAttack;
        public EntityWasHitEvent(Entity hitter, Entity victim, int comboCount) {
            this.hitter = hitter;
            this.victim = victim;
            this.comboCountOfAttack = comboCount;
        }
    }

    public static class ControllerPickedUpEvent implements Event {
        public Entity holder;
        public Entity controller;
        public ControllerPickedUpEvent(Entity holder, Entity controller) {
            this.holder = holder;
            this.controller = controller;
        }
    }

    public static class ScoringFinishedEvent implements Event {
        public Entity scorer;
        public long pointsGained;

        public ScoringFinishedEvent(Entity scorer, long pointsGained) {
            this.scorer = scorer;
            this.pointsGained = pointsGained;
        }
    }
    public static class ScoringTickEvent implements Event {
        public Entity scorer;
        public long pointsGained;
        public ScoringTickEvent(Entity scorer, long pointsGained)
        {
            this.scorer = scorer;
            this.pointsGained = pointsGained;
        }
    }

    public static class ControllerLostEvent implements Event {
        public Entity looser;
        public Entity controller;
        public ControllerLostEvent(Entity looser, Entity controller) {
            this.looser = looser;
            this.controller = controller;
        }
    }

    public static class ShowTextEvent implements Event{
        public int framesLeft = 600;
        public String s = "";
        public Entity initiator = null;
        public Vector2 pos = null;

        public ShowTextEvent(String text){
            this.s = text;
        }

        public ShowTextEvent(String text, int frames){
            this.s = text;
            this.framesLeft = frames;
        }

        public ShowTextEvent(String text, int frames, Entity textInitiator){
            this.s = text;
            this.framesLeft = frames;
            this.initiator = textInitiator;
        }


        public ShowTextEvent(String text, int frames, Vector2 pos){
            this.s = text;
            this.framesLeft = frames;
            this.pos = pos;
        }

    }




    public static class InputAttackPressedEvent implements Event {
        public Entity hitter;
        public InputAttackPressedEvent(Entity entity) {
            this.hitter = entity;
        }
    }

    public static class CallNamesEvent implements Event{
        public Entity hitter;
        public String str;
        public int framesLeft = 100;
        public CallNamesEvent(Entity entity, String called) {
            hitter = entity;
            str = called;
        }
    }

    public static class AnimationEvent implements Event {
        public Entity entity;
        public String name;
        public AnimationEvent(Entity entity, String name) {
            this.entity = entity;
            this.name = name;
        }
    }

    public static class AnimationOverEvent implements Event {
        public Entity entity;
        public String name;
        public AnimationOverEvent(Entity entity, String name) {
            this.entity = entity;
            this.name = name;
        }
    }



}

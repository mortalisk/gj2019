package no.gj.hg;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.math.Polygon;
import no.gj.hg.systems.utils;

public class EntityFactory {

    public static Entity createEntity(MapObject obj, float tileWidth) {
        MapProperties p = obj.getProperties();
        Float x = p.get("x", Float.class);
        Float y = p.get("y", Float.class);
        Float w = p.get("width", Float.class);
        Float h = p.get("height", Float.class);
        x = x/tileWidth;
        y = y/tileWidth;
        w = w/tileWidth;
        h = h/tileWidth;

        x += w *0.5f;
        y += h *0.5f;

        p.put("render.height", h);
        p.put("render.width", w);
        p.put("pos.x", x);
        p.put("pos.y", y);
        Entity e = new Entity(p);
        e.center = utils.getCenterPosition(e);
        e.isVisible = obj.isVisible();
        e.objectName = obj.getName();

        if (p.containsKey("textbox.hidden"))
        {
            e.isVisible = !(p.get("textbox.hidden", Boolean.class));
        }

        if (p.containsKey("isBorn")){
            e.isBorn = (p.get("isBorn", Boolean.class));
        }

        if (obj instanceof TiledMapTileMapObject)
        {
            TiledMapTileMapObject t = (TiledMapTileMapObject)obj;
            e.img = t.getTextureRegion().getTexture();
        }
        if (obj instanceof PolygonMapObject)
        {
            PolygonMapObject poly = (PolygonMapObject)obj;
            Polygon temp = poly.getPolygon();

            float[] vertices = temp.getVertices();
            float[] worldVertices = new float[vertices.length];

            for (int i = 0; i < vertices.length; ++i) {
                worldVertices[i] = vertices[i] / tileWidth;
            }

            Polygon polygon = new Polygon(worldVertices);
            e.polygon = polygon;

            p.put("phys.type", "polygon");
            p.put("phys.static", "yes");
        }
        return e;
    }
}

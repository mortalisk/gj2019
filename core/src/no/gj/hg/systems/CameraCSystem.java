package no.gj.hg.systems;

import com.badlogic.gdx.graphics.OrthographicCamera;
import no.gj.hg.Components;
import no.gj.hg.Entity;
import no.gj.hg.GameState;


public class CameraCSystem implements CSystem {

    Entity cameraSource;

    public CameraCSystem(GameState game) {
        game.camera = new OrthographicCamera(56, 31);
    }

    public void addEntity(Entity e) {
        if (e.has(Components.CAMERA))
        {
            cameraSource = e;
        }
    }

    public void update(GameState game) {
        if (cameraSource != null) {
            game.camera.position.set(cameraSource.get(Components.POS_X, Float.class), cameraSource.get(Components.POS_Y, Float.class), 0.0f);
            game.camera.update();
        }
    }


    public void dispose() {

    }

    public void removeEntity(Entity e) {
        if (e == cameraSource) cameraSource = null;
    }
}

package no.gj.hg.systems;


import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import no.gj.hg.Entity;
import no.gj.hg.GameState;

public class TileMapCSystem implements CSystem{

    TiledMap tiledMap;
    TiledMapRenderer tiledMapRenderer;

    public TileMapCSystem(TiledMap tileMap)
    {
        this.tiledMap = tileMap;
        Integer tileWidth = tiledMap.getProperties().get("tilewidth", Integer.class);
        float scale = 1.0f/tileWidth;
        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap, scale);
    }

    public void addEntity(Entity e) {
        //
    }


    public void update(GameState game) {
        tiledMapRenderer.setView(game.camera);
        tiledMapRenderer.render();
    }


    public void dispose() {

    }

    public void removeEntity(Entity e) {
    }
}

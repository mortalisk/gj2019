package no.gj.hg.systems;


import no.gj.hg.Entity;
import no.gj.hg.GameState;

import java.util.ArrayList;
import java.util.List;

public class TextboxCSystem implements CSystem {

    List<Entity> textBoxes = new ArrayList<Entity>();
    Entity player = null;

    float activationRange = 0.0f;

    public void addEntity(Entity e) {
        if (e.has("textbox.hidden"))
        {
            activationRange = e.get("textbox.range", Float.class);
            textBoxes.add(e);
        }
        if (e.has("input"))
        {
            player = e;
        }
    }

    public void update(GameState game) {

        for (Entity e : textBoxes) {
            if (e.get("textbox.hidden", Boolean.class)) {
                if (player.body.getPosition().dst(e.center) < activationRange) {
                    e.isVisible = true;
                } else {
                    e.isVisible = false;
                }
            }
        }
    }

    public void dispose() {

    }

    public void removeEntity(Entity e) {

    }
}

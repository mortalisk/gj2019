package no.gj.hg.systems;

import no.gj.hg.Event;
import no.gj.hg.GameState;

public interface EventSystem {
    void run(Event event, GameState game);
}

package no.gj.hg.systems;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import no.gj.hg.Components;
import no.gj.hg.Entity;
import no.gj.hg.Events;
import no.gj.hg.GameState;

import java.util.ArrayList;
import java.util.Random;

public class Physics implements CSystem, ContactListener {
    public static final short stat = 1;
    public static final short dyn = 2;
    Random rnd2 = new Random();

    boolean debug = false;

    Box2DDebugRenderer debugRenderer;
    ArrayList<Entity> players = new ArrayList<Entity>();

    Entity ball;
    Entity[] rackets = new Entity[2];
    //Entity[] controlPerson = new Entity[2];

    GameState game;

    public Physics(GameState game) {
        debugRenderer = new Box2DDebugRenderer();
        game.world = new World(new Vector2(0, 0), true);
        game.world.setContactListener(this);
        this.game = game;
    }

    public void addEntity(Entity e) {
        if (e.has(Components.IS_BORN)) {
            addBaby(e);
            players.add(e);
        } else if (e.propIs(Components.PHYS_TYPE, Components.CIRCLE)) {
            addCircle(e);
        } else if (e.propIs(Components.PHYS_TYPE, Components.POLYGON)) {
            addPolygon(e);
        }
        if (e.body != null) {
            e.body.setUserData(e);
        }
    }

    private void addBaby(Entity e) {
        float x = e.get("pos.x", Float.class);
        float y = e.get("pos.y", Float.class);
        float r = e.get("render.height", Float.class) / 5.0f;
        BodyDef bodyDef = new BodyDef();

        Vector2 pos = new Vector2(x, y);
        //float angle = e.get("rotation", Float.class);

        bodyDef.position.set(x, y);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.angularDamping = 1;
        bodyDef.fixedRotation = true;
        Body body = game.world.createBody(bodyDef);

        //TODO make baby parts have different friction for nice rotation


        // attack sensor

        FixtureDef sensorDef = new FixtureDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        sensorDef.filter.categoryBits = dyn;
        CircleShape sensorShape = new CircleShape();
        sensorShape.setRadius(r*2);
        sensorShape.setPosition(new Vector2(0.8f, 0));

        sensorDef.shape = sensorShape;
        sensorDef.density = 0.5f;
        sensorDef.friction = 0.4f;
        sensorDef.restitution = 0.6f;
        sensorDef.isSensor = true;
        Fixture sensor = body.createFixture(sensorDef);

        sensorShape.dispose();

        // head collision box

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.filter.categoryBits = dyn;
        CircleShape circle = new CircleShape();
        circle.setRadius(r);
        circle.setPosition(new Vector2(0.3f, 0));

        fixtureDef.shape = circle;
        fixtureDef.density = 0.5f;
        fixtureDef.friction = 0.4f;
        fixtureDef.restitution = 0.6f;
        body.createFixture(fixtureDef);

        circle.dispose();

        // butt collision box
        FixtureDef fixtureDefB = new FixtureDef();
        fixtureDefB.filter.categoryBits = dyn;

        CircleShape circleB = new CircleShape();
        circleB.setRadius(r);
        circleB.setPosition(new Vector2(-0.3f,0));

        fixtureDefB.shape = circleB;
        fixtureDefB.density = 0.5f;
        fixtureDefB.friction = 0.4f;
        fixtureDefB.restitution = 0.6f;
        Fixture fixtureB = body.createFixture(fixtureDefB);

        circleB.dispose();

        e.body = body;
    }


    private void addCircle(Entity e) {
        float x = e.get(Components.POS_X, Float.class);
        float y = e.get(Components.POS_Y, Float.class);
        float r = e.get(Components.RENDER_HEIGHT, Float.class) / 2.0f;
        BodyDef bodyDef = new BodyDef();
        FixtureDef fixtureDef = new FixtureDef();
        if (!e.has(Components.PHYS_STATIC)) {
            bodyDef.type = BodyDef.BodyType.DynamicBody;
            fixtureDef.filter.categoryBits = dyn;
        } else {
            bodyDef.type = BodyDef.BodyType.StaticBody;
            fixtureDef.filter.categoryBits = stat;
        }
        bodyDef.position.set(x, y);
        Body body = game.world.createBody(bodyDef);
        CircleShape circle = new CircleShape();
        circle.setRadius(r);

        fixtureDef.shape = circle;
        fixtureDef.density = 5f;
        if (e.has("onScreen")){
            fixtureDef.friction = 0.0f;
            fixtureDef.restitution = 1f;

            if (e.has("controllable")){
                fixtureDef.density = 1e7f;
                body.setLinearVelocity(0f,0f);
                if (e.has("racket")){
                    int rIndex = e.get("racket", int.class);
                    rackets[rIndex] = e;
                }
            }else {
                fixtureDef.density = 1e-7f;
                body.setLinearVelocity(2,0.1f);
                if (e.has("isBall")) {
                    ball = e;
                }
            }
        }else{
            fixtureDef.friction = 0.4f;
            fixtureDef.restitution = 0.6f;
            float linDamping = body.getLinearDamping();
            float angDamping = body.getAngularDamping();

            body.setLinearDamping(0.99f);
            body.setAngularDamping(0.99f);
        }



        Fixture fixture = body.createFixture(fixtureDef);

        circle.dispose();
        e.body = body;
    }

    private void addPolygon(Entity e) {
        float x = e.get(Components.POS_X, Float.class);
        float y = e.get(Components.POS_Y, Float.class);

        PolygonShape polygon = new PolygonShape();

        polygon.set(e.polygon.getVertices());

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;

        bodyDef.position.set(x, y);
        Body body = game.world.createBody(bodyDef);
        body.createFixture(polygon, 1);

        polygon.dispose();
        e.body = body;
    }

    public void update(GameState game) {

        if (debug) {
            debugRenderer.render(game.world, game.camera.combined);
        }
        for (Entity player : players) {
            //            if (!player.isVisible)
            //            {
            //                //do not process babies that do not exist yet
            //                //do not process babies that do not exist yet
            //                continue;
            //            }
            float a = player.body.getAngle();
            float speedconst = 0.9f;
            float x = (float)Math.cos(a) * speedconst;
            float y = (float)Math.sin(a) * speedconst;
            //float x = (float)Math.cos(a) * speedconst;
            //float y = (float)Math.sin(a) * speedconst;
            //try {

            player.body.setTransform(player.body.getPosition(), player.body.getAngle() + player.angleChange);
            if (player.itemHolderData.hasController){
                player.body.setLinearVelocity(0,0);
                player.body.setAngularVelocity(0);
                //player.body.setTransform(player.body.getPosition(), player.direction.angleRad());
            }else {
                if (!player.hitData.isSpinning) {
                    player.body.setLinearVelocity(x, y);
                }
            }
            //}catch (Exception e){
            //    System.out.println("We lost him");
            //}

        }

        updateBall();

        game.world.step(1 / 60f, 6, 2);
    }

    private static final float STARTING_BALL_VELOCITY = 5f;
    private static final float BALL_VELOCITY_INC = 0.01f;
    private float ballVelocity = STARTING_BALL_VELOCITY;

    private void updateBall(){
        Vector2 velocity = ball.body.getLinearVelocity();
        //FIXME some diversifing of pong
        if (Math.abs(velocity.y) < 0.02){
            velocity.y = Math.signum(velocity.y) * 0.02f;
        }
        if (Math.abs(velocity.x) < 0.01){
            velocity.x = Math.signum(velocity.x) * 0.01f;
        }

        float totalvelocity = (float) Math.hypot(velocity.x, velocity.y);
        //TODO set appropriate total velocity

        //lost ball
        //        if (ball.body.getPosition().x < rackets[0].body.getPosition().x ||
        //                ball.body.getPosition().x > rackets[1].body.getPosition().x){
        //        }

        velocity = new Vector2(velocity.x / totalvelocity * ballVelocity, velocity.y/ totalvelocity*ballVelocity);
        ball.body.setLinearVelocity(velocity);
        ballVelocity += BALL_VELOCITY_INC;

        Entity winner =null;
        if (ball.body.getPosition().x < rackets[0].body.getPosition().x){
            //ball.body.setLinearVelocity(Math.abs(velocity.x), velocity.y);

            resetBall();
            winner = findBabyWithController(1);
        }else
        if (ball.body.getPosition().x > rackets[1].body.getPosition().x){
            //ball.body.setLinearVelocity(-Math.abs(velocity.x), velocity.y);
            resetBall();
            winner = findBabyWithController(0);

        }

        if (winner != null) {
            game.addEvent(new Events.ScoringTickEvent(winner, 1));
        }
    }

    private Entity findBabyWithController(int racketID){
        for (Entity baby: players
             ) {
            if (baby.itemHolderData.hasController) {
                Entity controller = baby.itemHolderData.controller;
                int controllerID = controller.get(Components.ITEM_CONTROLLER_NUMBER, int.class);
                if (controllerID == racketID) {
                    return baby;
                }
            }
        }
        return null;
    }

    private void resetBall(){
        float newX = (rackets[0].body.getPosition().x + rackets[1].body.getPosition().x)/2;
        float newY = (rackets[0].body.getPosition().y + rackets[1].body.getPosition().y)/2;
        ball.body.setTransform(newX, newY, 0);
        ball.body.setLinearVelocity(rnd2.nextFloat()-0.5f, rnd2.nextFloat()-0.5f);
        ballVelocity = STARTING_BALL_VELOCITY;
    }

    public void dispose() {

    }

    public void removeEntity(Entity e) {
        if (e.body != null) {
            game.world.destroyBody(e.body);
            e.body = null;
        }
        if (players.contains(e)){
            players.remove(e);
        }
    }

    @Override public void beginContact(Contact contact) {

        Entity a = (Entity) contact.getFixtureA().getBody().getUserData();
        Entity b = (Entity) contact.getFixtureB().getBody().getUserData();
        if (contact.getFixtureA().isSensor() && !contact.getFixtureB().isSensor()) {
            game.addEvent(new Events.EnterSensorEvent(a, b));
        } else if (!contact.getFixtureA().isSensor() && contact.getFixtureB().isSensor()) {
            game.addEvent(new Events.EnterSensorEvent(b, a));
        }
    }

    @Override public void endContact(Contact contact) {
        Entity a = (Entity) contact.getFixtureA().getBody().getUserData();
        Entity b = (Entity) contact.getFixtureB().getBody().getUserData();
        if (contact.getFixtureA().isSensor() && !contact.getFixtureB().isSensor()) {
            game.addEvent(new Events.ExitSensorEvent(a, b));
        } else if (!contact.getFixtureA().isSensor() && contact.getFixtureB().isSensor()) {
            game.addEvent(new Events.ExitSensorEvent(b, a));
        }
    }

    @Override public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}

package no.gj.hg.systems;

import no.gj.hg.*;

import java.util.ArrayList;
import java.util.List;

// Keeps track of scoring
public class ScoringSystem  implements EventSystem, CSystem{

    long previousTick = 0;
    List<Entity> babies = new ArrayList<Entity>();

    public class ScoringData {
        long totalScore = 0;
        long scoreTimerStart = 0;
    }

    @Override
    public void addEntity(Entity e) {
        // only characters can score
        if (e.has(Components.CHARACTER))
        {
            e.scoringData = new ScoringData();
            babies.add(e);
        }
    }

    @Override
    public void update(GameState game)
    {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void removeEntity(Entity e) {

    }

    @Override
    public void run(Event event, GameState game) {
        if (event instanceof Events.ScoringTickEvent) {
            Events.ScoringTickEvent e = (Events.ScoringTickEvent) event;
            scoreBaby(game, e.scorer, e.pointsGained);
        }
    }

    private void scoreBaby(GameState game, Entity baby, long score) {
        System.out.println("scoreBaby");
        baby.scoringData.totalScore += score;
        // score some points
        game.addEvent(new Events.ScoringFinishedEvent(baby, score));
        game.addEvent(SoundSystem.play("score", 1));
    }
}

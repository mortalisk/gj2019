package no.gj.hg.systems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.TimeUtils;
import no.gj.hg.Entity;
import no.gj.hg.Event;
import no.gj.hg.Events;
import no.gj.hg.GameState;

import java.util.*;

public class HitSystem implements EventSystem, CSystem {
    List<Entity> entities = new ArrayList<Entity>();
    int frameCounter = 0;
    Random rnd = new Random();

    public class HitData {
        Set<Entity> inside = new HashSet<Entity>();
        boolean hitAnimationInProgress = false;
        public long lastHitAnimationOverTime;
        int comboCount = 0;
        boolean isSpinning = false;
        int framesSinceLastHit = 0;
        float angleChangeAfterHit = 0;
    }




    @Override public void addEntity(Entity e) {
        if (e.body == null) return;
        for (Fixture f : e.body.getFixtureList()) {
            if(f.isSensor()) {
                e.hitData = new HitData();
                entities.add(e);
            }
        }
    }

    @Override public void update(GameState game) {
        for (Entity e : entities) {
            for (Entity hit: e.hitData.inside) {
                //System.out.println(e.toString() + " is hitting " + hit);
            }
            if (e.hitData.isSpinning) {
                e.hitData.framesSinceLastHit++;
                e.body.setTransform(e.body.getPosition(), e.hitData.angleChangeAfterHit);
            }

            if (e.hitData.framesSinceLastHit > 120) {
                e.hitData.isSpinning = false;
                e.hitData.framesSinceLastHit = 0;
                e.hitData.angleChangeAfterHit = 0;
            }
        }
    }

    @Override public void dispose() {

    }

    @Override public void removeEntity(Entity e) {
        entities.remove(e);
    }

    @Override public void run(Event event, GameState game) {
        if( event instanceof Events.EnterSensorEvent) {
            Events.EnterSensorEvent e = (Events.EnterSensorEvent) event;
            e.senser.hitData.inside.add(e.entered);
        } else if (event instanceof Events.ExitSensorEvent) {
            Events.ExitSensorEvent e = (Events.ExitSensorEvent) event;
            e.senser.hitData.inside.remove(e.exited);
        } else if (event instanceof Events.InputAttackPressedEvent) {
            Events.InputAttackPressedEvent e = (Events.InputAttackPressedEvent) event;
            HitData hitData = e.hitter.hitData;

            long time = TimeUtils.millis();
            if (!hitData.hitAnimationInProgress) {
                if ( time - hitData.lastHitAnimationOverTime < 500) {
                    hitData.comboCount++;
                    if (hitData.comboCount > 3) {
                        hitData.comboCount = 1;
                    }
                } else {
                    hitData.comboCount = 1;
                }
                game.addEvent(new Events.AnimationEvent(e.hitter, "attack" + hitData.comboCount));
                game.addEvent(SoundSystem.play("attack" + hitData.comboCount, 1));
                hitData.hitAnimationInProgress = true;
            } else {
                hitData.comboCount = 0;
            }
        } else if (event instanceof Events.AnimationOverEvent) {
            Events.AnimationOverEvent e = (Events.AnimationOverEvent)event;
            if (e.entity.hitData.hitAnimationInProgress) {
                e.entity.hitData.hitAnimationInProgress = false;
                long time = TimeUtils.millis();
                e.entity.hitData.lastHitAnimationOverTime = time;
                for (Entity victim : e.entity.hitData.inside) {
                    game.addEvent(new Events.EntityWasHitEvent(e.entity, victim, e.entity.hitData.comboCount));
                }
            }
        } else if (event instanceof Events.EntityWasHitEvent) {
            Events.EntityWasHitEvent e = (Events.EntityWasHitEvent) event;
            if (e.victim.hitData != null) {
                e.victim.hitData.comboCount = 0;
                //e.victim.hitData.isSpinning = true;
                boolean left = rnd.nextBoolean();
                float angle = 0.1f * (left ? -1 : 1);
                e.victim.hitData.angleChangeAfterHit = 0; // removed this

                //FIXME does the impulse work???
                Vector2 dir = e.victim.body.getPosition().cpy().sub(e.hitter.body.getPosition()).nor();
                e.victim.body.applyLinearImpulse(dir, e.victim.body.getPosition(), true);

                Entity victim = e.victim;
                int comboCountOfAttack = e.comboCountOfAttack;
                if (victim.itemHolderData != null) {
                    if ((!victim.itemHolderData.hasController)
                            || (victim.itemHolderData.hasController && comboCountOfAttack == 3)) {
                        game.addEvent(new Events.AnimationEvent(e.victim, "hurt"));
                    }
                }
            }
        }
    }


}

package no.gj.hg.systems;

import no.gj.hg.Entity;
import no.gj.hg.GameState;

public interface CSystem {
    void addEntity(Entity e);
    void update(GameState game);
    void dispose();
    void removeEntity(Entity e);
}

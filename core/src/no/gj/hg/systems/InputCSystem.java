package no.gj.hg.systems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import no.gj.hg.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class InputCSystem implements CSystem , EventSystem{

    private Entity[] rackets = new Entity[2];
    private boolean[] wasControlled;
    private Entity ball;

    @Override
    public void run(Event event, GameState game) {
        if (event instanceof Events.EntityWasHitEvent){
            Entity victim = ((Events.EntityWasHitEvent) event).victim;
            Entity hitter = ((Events.EntityWasHitEvent) event).hitter;
            //TODO Sergey: I think we should change controls of the player with a joystick
            //may be not every time
            if (victim.has("isBorn")
                //        && !victim.itemHolderData.hasController
                ){
                int luck = rnd.nextInt(6) + 1; //trowing 6 sided die
                if (victim.itemHolderData.hasController){
                    luck += 1;
                }
                if (luck<=3) {
                    String s = messUpPlayer(hitter, victim);
                    game.addEvent(new Events.CallNamesEvent(hitter, s));
                }
            }

        }
    }

    public static class InputData {
        long aButtonPressTime = 0;
        long bButtonPressTime = 0;
    }


    private List<String>[] nameLists = new List[TOTAL_KEYS];
    private List<String>[] nickLists = new List[TOTAL_KEYS];




    public InputCSystem() {
        populateArrayOfListsFromFile(nameLists,"names/female.txt");
        populateArrayOfListsFromFile(nickLists, "names/nicknames.txt");
    }

    private void populateArrayOfListsFromFile(List<String>[] lists, String fileName){
        // The name of the file to open.


        // This will reference one line at a time
        String line = null;

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader =
                    new FileReader(fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader =
                    new BufferedReader(fileReader);

            char letter = 'A'-1;
            while((line = bufferedReader.readLine()) != null) {
                while (line.charAt(0)>letter){
                    letter++;
                    lists[letter-'A'] = new ArrayList<String>();
                }
                lists[letter-'A'].add(line);
            }

            // Always close files.
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        }
        catch(IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + fileName + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }
        // The name of the file to open.
        System.out.println("read " + fileName );

    }

    static private final String[] KEY_LINES = {
            "Q  W  E  R  T  Y  U  I O P",
            " A  S  D  F  G  H  J  K L",
            "  Z  X  C  V  B  N  M"};



    static private final int BUTTON_SHIFT_GDX = 36;
    static private final int TOTAL_KEYS = 26;
    List<Entity> entities = new ArrayList<Entity>();
    boolean[] usedButtons = new boolean[TOTAL_KEYS];
    Random rnd = new Random();

    @Override
    public void addEntity(Entity e) {
        if (e.has(Components.BUTTONS)) {
            //System.out.println((int)e.get(Components.BUTTONS, String.class).charAt(0));
            long t1 = System.currentTimeMillis();
            String newButtons = reserveButtons();
            long dt = System.currentTimeMillis() - t1;
            System.out.println("Choosing keyboard time: " + dt);


            t1 = System.currentTimeMillis();
            e.name = generateName(newButtons);
            dt = System.currentTimeMillis() - t1;
            System.out.println("Generate name     time: " + dt);

            e.inputData = new InputData();
            newButtons = switchButtonsIfNeeded(newButtons.toCharArray());
            e.set(Components.BUTTONS, newButtons);
            entities.add(e);
        }
        if (e.has("racket")){
            int rIndex = e.get("racket", int.class);
            rackets[rIndex] = e;
        }
        if (e.has("isBall")) {
            ball = e;
        }
    }

    @Override
    public void removeEntity(Entity e) {
        entities.remove(e);
        tryReleaseButtons(e);
    }

    private int findKeyPos(char c){
        for (String s : KEY_LINES) {
            if (s.contains(c+"")){
                return s.indexOf(c);
            }
        }
        return -1;
    }

    private String switchButtonsIfNeeded(char[] buttons){
        if (findKeyPos(buttons[0]) > findKeyPos(buttons[1])){
            return buttons[1] + "" + buttons[0];
        }
        else
            return buttons[0] + "" + buttons[1];
    }


    private char reserveButton(){
        char buttonSug = '0';
        while (true) {
            int sugestion = rnd.nextInt(TOTAL_KEYS);
            if (!usedButtons[sugestion]) {
                usedButtons[sugestion] = true;
                buttonSug = (char) (sugestion + 'A');
                return buttonSug;
            }
        }
    }

    private String reserveButtons(){
        char[] buttonSug = new char[2];
        for (int i = 0; i < 2; ++i) {
            buttonSug[i] = reserveButton();
        }

        String buttons = new String(buttonSug);
        return buttons;
    }

    private void tryReleaseButton(char button){
        usedButtons[button-'A'] = false;
    }

    private void tryReleaseButtons(Entity e){
        if (e.has(Components.BUTTONS)){
            String buttons = e.get(Components.BUTTONS, String.class);
            for (char button: buttons.toCharArray()) {
                tryReleaseButton(button);
            }
        }
    }

    private String generateName(char c){
        return nameLists[c-'A'].get(rnd.nextInt(nameLists[c-'A'].size()));
    }

    private String generateNick(char c){
        return nickLists[c-'A'].get(rnd.nextInt(nickLists[c-'A'].size()));
    }


    private String generateName(String initials){
        return generateNick(initials.charAt(0)) + " " + generateName(initials.charAt(1));
    }

    public void update(GameState game) {

        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            Gdx.app.exit();
        }

        wasControlled = new boolean[2];

        if (justStarted){
            game.events.add(new Events.ShowTextEvent("== Night Crawlers ==", 900));
            game.events.add(new Events.ShowTextEvent("Game from GGJ'19 by", 900));
            game.events.add(new Events.ShowTextEvent("Marius, Morten,", 900));
            game.events.add(new Events.ShowTextEvent("Sergey, Weronika", 900));
            game.events.add(new Events.ShowTextEvent("ENTER to add crawlers", 1800));
            justStarted = false;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)){
            if (!game.unbornBabies.isEmpty()) {
                Entity baby = game.unbornBabies.pop();
                game.toAdd.add(baby);
                if (!playersWereAddedBefore){
                    game.events.add(new Events.ShowTextEvent("Welcome     ", 1800));
                    game.events.add(new Events.ShowTextEvent("Night Crawler  ", 1800));
                    game.events.add(new Events.ShowTextEvent("Use initials to turn", 2500));
                    game.events.add(new Events.ShowTextEvent("Use both to hit", 2500));
                    game.events.add(new Events.ShowTextEvent("Time hits for combo", 2500));
                    game.events.add(new Events.ShowTextEvent("Pass this knowledge!",2500));
                    playersWereAddedBefore = true;
                    //game.events.add(new Events.ShowTextEvent("Pass this knowledge!",2400));
                }
            }
        }
        //TODO comented lines show coordinate mesh for debugging
        //        if (Gdx.input.isKeyJustPressed(Input.Keys.TAB))
        //        {
        //            for (int y = 0; y < 100; y += 5){
        //                for (int x = 0; x<100; x+= 10){
        //                    game.events.add(new Events.ShowTextEvent(x+","+y, 6000, new Vector2(x,y)));
        //                }
        //            }
        //        }

        //TODO remove the least active player
        if (Gdx.input.isKeyJustPressed(Input.Keys.DEL)){
            for (Entity ent: entities) {
                if (ent.has("isBorn")){
                    game.unbornBabies.add(ent);
                    game.toRemove.add(ent);
                    break;
                }
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.END)){
            int messUpInd = rnd.nextInt(entities.size());
            Entity e = entities.get(messUpInd);
            messUpPlayer(e);
            //e.set(but);
        }

        for (Entity e: entities) {
            String buttons = e.get(Components.BUTTONS, String.class);

            boolean abutton = Gdx.input.isKeyPressed(buttons.charAt(0) -BUTTON_SHIFT_GDX);
            boolean bbutton = Gdx.input.isKeyPressed(buttons.charAt(1) -BUTTON_SHIFT_GDX);
            boolean ajustpressed = Gdx.input.isKeyJustPressed(buttons.charAt(0) -BUTTON_SHIFT_GDX);
            boolean bjustpressed = Gdx.input.isKeyJustPressed(buttons.charAt(1) -BUTTON_SHIFT_GDX);



            long time = TimeUtils.millis();
            if (ajustpressed || bjustpressed) {
                if (ajustpressed) {
                    e.inputData.aButtonPressTime = time;
                }
                if (bjustpressed) {
                    e.inputData.bButtonPressTime = time;
                }

                if (ajustpressed && time - e.inputData.bButtonPressTime < 200
                    ||
                    bjustpressed && time - e.inputData.aButtonPressTime < 200) {
                    //TODO consider doing something else when you have a controller
                    //for examle go to rotation mode
                    if (!e.itemHolderData.hasController) {
                        game.addEvent(new Events.InputAttackPressedEvent(e));
                    }
                    //TODO the code for call names is bellow
                    //String s = messUpPlayer(e, s);
                    //String s = messUpPlayer(e, s);
                    //game.addEvent(new Events.CallNamesEvent(e, s));
                }
            }


            e.angleChange = 0;
            //player rotation
            if (e.itemHolderData.hasController){
                e.body.setTransform(e.body.getPosition(), (float)Math.PI/2);Entity controller = e.itemHolderData.controller;
                int racketID = controller.get("item.controller_number", int.class);
                wasControlled[racketID] = true;
                Entity racket = rackets[racketID];
                moveRacket(racket, abutton, bbutton);
            }else {
                if (abutton && time - e.inputData.aButtonPressTime > 20 && !bbutton) {
                    e.angleChange = 0.1f;
                } else if (bbutton && time - e.inputData.bButtonPressTime > 20 && !abutton) {
                    e.angleChange = -0.1f;
                }
            }


        }
        //end of loop

        //pong movement AI
        for (int i =0 ; i < rackets.length; i++){
            Vector2 pos = rackets[i].body.getPosition();
            float y = pos.y;
            y = Math.min(y , TOP_SCREEN);
            y = Math.max(y , BOTTOM_SCREEN);
            rackets[i].body.setTransform(pos.x, y, 0);
            if (!wasControlled[i]){
                applyAI(rackets[i], ball);
            }
        }

    }

    public void applyAI(Entity racket, Entity ball){
        float rndOvershoot = 1.2f;
        if (racket.body.getPosition().y > ball.body.getPosition().y + RACKET_MOVEMENT_SPEED* rndOvershoot){
            //for (int j = 0; j < rnd.nextInt(5); ++j) {
            moveRacket(racket, false, true);
            //}
        }
        if (racket.body.getPosition().y < ball.body.getPosition().y - RACKET_MOVEMENT_SPEED * rndOvershoot){
            //for (int j = 0; j < rnd.nextInt(5); ++j) {
            moveRacket(racket, true, false);
            //}
        }
    }

    float RACKET_MOVEMENT_SPEED = 0.2f;
    private static final float TOP_SCREEN = 86f;
    private static final float BOTTOM_SCREEN = 80f;
    boolean playersWereAddedBefore = false;
    boolean justStarted = true;

    public void moveRacket(Entity racket, boolean buttonA, boolean buttonB){
        Vector2 pos = racket.body.getPosition();

        if (buttonA && buttonB){
            return;
        }
        //TODO check direction of light
        if (buttonA){
            racket.body.setTransform(pos.x,
                    Math.max(pos.y + RACKET_MOVEMENT_SPEED, BOTTOM_SCREEN),
                    (float)(-Math.PI/2));
        } else if (buttonB){
            racket.body.setTransform(pos.x,
                    Math.min(pos.y - RACKET_MOVEMENT_SPEED, TOP_SCREEN),
                    (float)(-Math.PI/2));
        }

    }

    public String messUpPlayerNotMe1(Entity e){
        int ind = rnd.nextInt(entities.size());
        Entity otherPlayer = entities.get(ind);
        String oldName = otherPlayer.name;
        if (e != otherPlayer){
            return oldName.substring(oldName.indexOf(" "))  + "! you! " +messUpPlayer(otherPlayer) + "!";
        }
        return "";
    }

    public String messUpPlayer(Entity atacker, Entity victim){
        Entity otherPlayer = victim;
        String oldName = otherPlayer.name;

        return oldName.substring(oldName.indexOf(" "))  + "! you! " +messUpPlayer(otherPlayer) + "!";


    }


    public String messUpPlayer(Entity e){
        String nick = e.name;
        e.name = e.name.substring(e.name.indexOf(' ')+1);
        //first char of old nick
        char old = nick.charAt(0);
        tryReleaseButton(old);
        //first char of old name
        old = e.name.charAt(0);
        char newButton = reserveButton();
        nick = generateNick(newButton);
        e.name = nick + " " + e.name;
        char[] newButtons = {newButton, old};
        String buttons = switchButtonsIfNeeded(newButtons);
        e.set(Components.BUTTONS, buttons);
        return nick;
    }

    @Override
    public void dispose() {

    }


}

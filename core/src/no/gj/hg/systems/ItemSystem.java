package no.gj.hg.systems;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import no.gj.hg.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ItemSystem implements EventSystem, CSystem {
    Random rnd = new Random();

    @Override
    public void addEntity(Entity e) {
        if (e.has(Components.ITEM_CONTROLLER)) {
            e.body.setLinearDamping(1);
        }
        // Adds itemdata stuff
        if (e.body == null) return;
        for (Fixture f : e.body.getFixtureList()) {
            if(f.isSensor()) {
                e.itemHolderData = new ItemHolderData();
            }
        }
        if (e.has(Components.ITEM_CONTROLLER))
        {
            e.itemData = new ItemData();
            e.itemData.controllerNumber = e.get(Components.ITEM_CONTROLLER_NUMBER, Integer.class);
        }
    }

    @Override
    public void update(GameState game) {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void removeEntity(Entity e) {
    }

    public class ItemData {
        Entity holder;
        int controllerNumber;
        boolean isHeld = false;
    }

    public class ItemHolderData {
        Entity controller;
        boolean hasController;
//        /**
//         * -1 no controller
//         * 0 controller 0
//         * 1 controller 1
//         */
//        int hasController = -1;
    }


    @Override
    public void run(Event event, GameState game) {

        if( event instanceof Events.EnterSensorEvent) {
            Events.EnterSensorEvent e = (Events.EnterSensorEvent) event;
            if (e.entered.has(Components.ITEM_CONTROLLER))
            {
                System.out.println("Pickup controller!!!");
                Entity baby = e.senser;
                Entity controller = e.entered;

                // only one can hold controller
                if (!controller.itemData.isHeld) {
                    baby.itemHolderData.controller = controller;
                    baby.itemHolderData.hasController = true;
                    baby.body.setType(BodyDef.BodyType.StaticBody);
                    controller.itemData.holder = baby;
                    controller.itemData.isHeld = true;

                    // maybe also need to do something somewhere??
                    controller.isVisible = false;
                    // maybe this removes it from physics?
                    if (controller.body != null) {
                        controller.body.setActive(false);
                    }

                    game.addEvent(new Events.ControllerPickedUpEvent(baby, controller));
                    game.addEvent(new Events.AnimationEvent(e.senser, "play"));
                    game.addEvent(SoundSystem.play("pickup", 0.5f));
                }
            }
        }
        if (event instanceof Events.EntityWasHitEvent) {
            Events.EntityWasHitEvent e = (Events.EntityWasHitEvent) event;
            Entity hitter = e.hitter;
            Entity victim = e.victim;
            int comboCountOfAttack = e.comboCountOfAttack;
            if (victim.itemHolderData != null) {
                if (victim.itemHolderData.hasController && comboCountOfAttack == 3) {
                    // loose controller
                    Entity controller = victim.itemHolderData.controller;
                    victim.itemHolderData.hasController = false;
                    victim.body.setType(BodyDef.BodyType.DynamicBody);
                    controller.itemData.holder = null;
                    controller.itemData.isHeld = false;
                    controller.isVisible = true;
                    if (controller.body != null) {
                        Vector2 hitterPos = hitter.body.getPosition().cpy();
                        Vector2 throwDir = hitterPos.cpy().sub(victim.body.getPosition());
                        Vector2 newPos = hitterPos.add(throwDir.cpy().scl(3));
                        if (newPos.x > 40) newPos.x = 40;
                        if (newPos.x < 8) newPos.x = 8;
                        if (newPos.y > 80) newPos.y = 80;
                        if (newPos.y < 60) newPos.y = 60;
                        controller.body.setTransform(newPos, 0);
                        controller.body.setActive(true);
                        float angle = throwDir.angleRad();
                        float impulsex = (float)Math.cos(angle) * 5;
                        float impulsey = (float)Math.sin(angle) * 5;
                        // generate some random force
                        controller.body.applyLinearImpulse(impulsex, impulsey, 0,0, true);
                    }

                    // victim is not holding controller anymore
                    game.addEvent(new Events.AnimationEvent(victim, "crawl"));
                    game.addEvent(new Events.ControllerLostEvent(victim, controller));
                }
            }
        }
    }
}

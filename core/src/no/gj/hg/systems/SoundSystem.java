package no.gj.hg.systems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import no.gj.hg.Event;
import no.gj.hg.GameState;

import java.util.HashMap;
import java.util.Map;

public class SoundSystem implements EventSystem {

    private final Map<String, Sound> sounds = new HashMap<String, Sound>();

    public SoundSystem() {
        sounds.put("music", Gdx.audio.newSound(Gdx.files.internal("music/empty_dipers_mixdown_ballanced.mp3")));
        // Add controller pickup sound
        sounds.put("score", Gdx.audio.newSound(Gdx.files.internal("sound/score_mixdown.mp3")));
        sounds.put("attack1", Gdx.audio.newSound(Gdx.files.internal("sound/kick2_mixdown.mp3")));
        sounds.put("attack2", Gdx.audio.newSound(Gdx.files.internal("sound/punch_mixdown.mp3")));
        sounds.put("attack3", Gdx.audio.newSound(Gdx.files.internal("sound/doublepunch_mixdown.mp3")));
        sounds.put("pickup", Gdx.audio.newSound(Gdx.files.internal("sound/evillough_mixdown.mp3")));
    }

    @Override public void run(Event event, GameState game) {
        if (event instanceof SoundEvent) {
            SoundEvent e = (SoundEvent) event;
            Sound sound = sounds.get(e.name);
            if (sound != null) {
                if (e.action.equals("loop")) {
                    float vol = e.volume;
                    sound.loop(vol);
                }
                if (e.action.equals("play")) {
                    float vol = e.volume;
                    sound.play(vol);
                }else if (e.action.equals("stop")) {
                    sound.stop();
                }
            }
        }
    }

    private static class SoundEvent implements Event {
        String name;
        String action;
        float volume;
    }

    public static Event loop(String s, float vol) {
        SoundEvent e = new SoundEvent();
        e.name = s;
        e.action = "loop";
        e.volume = vol;
        return e;
    }

    public static Event play(String s, float vol) {
        SoundEvent e = new SoundEvent();
        e.name = s;
        e.action = "play";
        e.volume = vol;
        return e;
    }

    public static Event loop(String s) {
        return loop(s, 1);
    }

    public static Event stop(String s) {
        SoundEvent e = new SoundEvent();
        e.name = s;
        e.action = "loop";
        e.volume = 1;
        return e;
    }
}

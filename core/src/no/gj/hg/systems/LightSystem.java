package no.gj.hg.systems;

import box2dLight.ConeLight;
import box2dLight.PointLight;
import box2dLight.PositionalLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Filter;
import no.gj.hg.Components;
import no.gj.hg.Entity;
import no.gj.hg.GameState;
public class LightSystem implements CSystem {

    RayHandler rayHandler;
    Entity player;
    ConeLight playerGunLight;

    public LightSystem(GameState game) {
        rayHandler = new RayHandler(game.world);
        rayHandler.setAmbientLight(0.5f);
    }

    @Override public void addEntity(Entity e) {
        if (e.propIs(Components.LIGHT_TYPE, Components.LIGHT_POINT)) {
            addPointLight(e);
        }
        else if(e.propIs(Components.LIGHT_TYPE, Components.LIGHT_CONE)) {
            addConeLight(e);
        }
        if(e.has("gun"))
        {
            player = e;
            float distance = 15;
            Vector2 playerPos = utils.getCenterPosition(e);
            playerGunLight = new ConeLight(rayHandler, 100, new Color(0xFF8888FF),
                    distance, playerPos.x, playerPos.y, -90, 30);
            playerGunLight.setXray(true);
            playerGunLight.attachToBody(e.body);
        }
    }

    @Override public void update(GameState game) {
        shootGun(player);
        rayHandler.setCombinedMatrix(game.camera);
        rayHandler.updateAndRender();
    }

    @Override public void dispose() {

    }

    @Override public void removeEntity(Entity e) {

    }

    private void shootGun(Entity gunner) {
        if(gunner == null)
            return;

        // adjust gun light
        //float deg = player.direction.angle();
        //

        // adjust gun light color
        Color color;
        float coneDeg;
        float distance;
        if(gunner.isGunActive)
        {
            coneDeg = 5;
            distance = 100;
            assert(gunner.has("gun.lightcolor.on"));
            color = getColor(gunner, "gun.lightcolor.on");
        }
        else
        {
            coneDeg = 30;
            distance = 15;
            assert(gunner.has("gun.lightcolor.off"));
            color = getColor(gunner, "gun.lightcolor.off");
        }
        playerGunLight.setColor(color);
        playerGunLight.setConeDegree(coneDeg);
        playerGunLight.setDistance(distance);
    }

    private void addConeLight(Entity e) {

        if (!e.isVisible) return;
        assert(e.has(Components.LIGHT_COLOR));
        assert(e.has(Components.LIGHT_DISTANCE));
        assert(e.has(Components.LIGHT_DIRECTION_DEG));
        assert(e.has(Components.LIGHT_CONE_DEG));

        Color color = getColor(e, Components.LIGHT_COLOR);
        Float distance = e.get(Components.LIGHT_DISTANCE, Float.class);
        Vector2 pos = utils.getCenterPosition(e);
        Float directionDegree = e.get(Components.LIGHT_DIRECTION_DEG, Float.class);
        Float coneDegree = e.get(Components.LIGHT_CONE_DEG, Float.class);

        // rays per degree
        int rays = (int)Math.round(Math.ceil(coneDegree)) * 5;

        PositionalLight l = new ConeLight(rayHandler, rays, color, distance, pos.x, pos.y, directionDegree, coneDegree);
        if(e.body != null)
            l.attachToBody(e.body);
    }

    // rays per degree
    int RAYS_NUM = 360 * 5;
    private void addPointLight(Entity e) {
        if (!e.isVisible) return;
        assert(e.has(Components.LIGHT_COLOR));
        assert(e.has(Components.LIGHT_DISTANCE));
        assert(e.has(Components.LIGHT_STATIC));

        Color color = getColor(e, Components.LIGHT_COLOR);
        Float distance = e.get(Components.LIGHT_DISTANCE, Float.class);
        Vector2 pos = utils.getCenterPosition(e);
        boolean isStaticOnly = e.has(Components.LIGHT_STATIC) ? e.get(Components.LIGHT_STATIC, Boolean.class) : false;

        PositionalLight l = new PointLight(rayHandler, RAYS_NUM, color, distance, pos.x, pos.y);
        if(isStaticOnly) {
            Filter f = new Filter();
            f.maskBits = Physics.stat;
            l.setContactFilter(f);
        }

        if(e.body != null)
            l.attachToBody(e.body);
    }

    private Color getColor(Entity e, String key) {
        return new Color((int)Long.parseLong(e.get(key, String.class), 16));
    }

}

package no.gj.hg.systems;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;
import no.gj.hg.Entity;

/**
 * Created by ato on 21.01.17.
 */
public class utils {
    public static Vector2 getCenterPosition(Entity entity)
    {
        Float x = entity.get("pos.x", Float.class);
        Float y = entity.get("pos.y", Float.class);
        Float width = entity.get("render.width", Float.class);
        Float height = entity.get("render.height", Float.class);

        Vector2 v = new Vector2(x, y);
        return v;
    }
}

package no.gj.hg.systems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import editor.character.*;
import editor.character.Character;
import no.gj.hg.*;

import java.util.ArrayList;
import java.util.List;


public class RenderingCSystem implements CSystem, EventSystem {

    GameState game;
    BitmapFont font;

    private static final float TV_TOP = 86.2f;
    private static final float TV_BOTTOM = 81f;
    private static final float TV_LEFT = 13f;


    private class Message{
        Vector2 pos = null;
        Message(Events.ShowTextEvent event){
            if (event.pos!=null){
                pos = event.pos;
            }else if (event.initiator != null && event.initiator.body != null) {
                Vector2 oldPos = event.initiator.body.getPosition();
                pos = new Vector2(oldPos.x-6, oldPos.y+TEXT_Y_SHIFT *2);
            }else {
                defaultPosition = true;
            }
            s = event.s;
            framesLeft = event.framesLeft;

        }

        Message(String ss, Vector2 position){
            s = ss;
            pos = position;
        }

        Message(String ss, Vector2 position, int framesToShow){
            s = ss;
            pos = position;
            framesLeft = framesToShow;
        }

        Message(String ss, int framesToShow){
            s = ss;
            defaultPosition = true;
            framesLeft = framesToShow;
        }

        int framesLeft = 100;
        String s = "";
        boolean defaultPosition = false;

    }

    List<Message> messages = new ArrayList<Message>();

    public static class RenderData {
        Character character = null;
        private int loopAnimation = 1;
        private int currentAnimation = 1;
        private int currentFrame = 0;
        private int displayCount = 0;

        void setCurrentAnimation(int i) {
            if (((Animation)character.getChild(i)).isLoop()) {
                loopAnimation = i;
            }
            currentAnimation = i;
            currentFrame = 0;
            displayCount = 0;
        }

        /** returns whether animation is done */
        public boolean incrementFrame() {
            int fps = character.getFps();
            Animation animation = (Animation)character.getChild(currentAnimation);
            int numberOfFrames = animation.getChildCount();
            int rendersPerFrame = 60 / fps;
            boolean done = false;
            displayCount++;
            if (displayCount > rendersPerFrame) {
                displayCount = 0;
                currentFrame++;
                if (currentFrame >= numberOfFrames) {
                    currentFrame = 0;
                    if (!animation.isLoop()) {
                        currentAnimation = loopAnimation;
                        done = true;
                    }
                }
            }
            return done;
        }

        public Animation getCurrentAnimation() {
            return (Animation)character.getChild(currentAnimation);
        }

        public BodyNode getNodeToDraw() {
            Animation animation = (Animation)character.getChild(currentAnimation);
            return (BodyNode)animation.getChild(currentFrame).getChild(0);
        }

        public boolean isLooping() {
            Animation animation = (Animation)character.getChild(currentAnimation);
            return animation.isLoop();
        }
    }



    @Override public void run(Event event, GameState game) {
        if (event instanceof Events.AnimationEvent) {
            Events.AnimationEvent e = (Events.AnimationEvent) event;
            for (int i = 0; i < e.entity.renderData.character.getChildCount(); i++) {
                Node n = e.entity.renderData.character.getChild(i);
                if (n.getName().equals(e.name)) {
                    e.entity.renderData.setCurrentAnimation(i);
                }
            }
        }
        if (event instanceof Events.CallNamesEvent){
            Events.CallNamesEvent event2 = (Events.CallNamesEvent) event;
            game.events.add(new Events.ShowTextEvent(event2.str, 200, event2.hitter));
            //messages.add(new Message(event2.str, event2.hitter.body.getPosition()));
        }
        if (event instanceof Events.ShowTextEvent){
            Events.ShowTextEvent event2 = (Events.ShowTextEvent) event;
            messages.add(new Message(event2));
        }

        if (event instanceof Events.ScoringFinishedEvent)
        {
            Events.ScoringFinishedEvent e = (Events.ScoringFinishedEvent) event;
            Vector2 outputPos = e.scorer.body.getPosition().add(0.0f, 10.0f);
            messages.add(new Message("" + e.pointsGained, outputPos, 60));
        }
    }

    private void initFont(){
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/FredokaOne-Regular.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 50;
        font = generator.generateFont(parameter);
        font.setUseIntegerPositions(false);
        generator.dispose();
    }

    public RenderingCSystem(GameState game) {
        //TODO fix the scale
        initFont();
        //TrueTypeFontFactory.createBitmapFont(Gdx.files.internal("font.ttf"), FONT_CHARACTERS, 12.5f, 7.5f, 1.0f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        font.setColor(1f, 1f, 1f, 0.8f);
        font.getData().setScale(0.03f);
        //font.getData().
        this.game = game;
    }



    SpriteBatch batch = new SpriteBatch();
    List<Entity> entities = new ArrayList<Entity>();


    public void addEntity(Entity entity) {
        RenderData data = new RenderData();
        entity.renderData = data;
        if (entity.img != null) {
            entities.add(entity);
        }
        entity.width = entity.get(Components.RENDER_WIDTH, Float.class);
        entity.height = entity.get(Components.RENDER_HEIGHT, Float.class);
        if (entity.img == null && entity.has(Components.RENDER_TEXTURE)) {
            entity.img = new Texture(entity.get(Components.RENDER_TEXTURE, String.class));
            entities.add(entity);
        }
        if (entity.has(Components.CHARACTER)) {
            String name = entity.get(Components.CHARACTER, String.class);
            String path = "chars/" + name;
            Character character = CharacterXML.open(path, name, new ImageLoader() {
                @Override public Object loadImage(String s, String s1) {
                    return new Texture(Gdx.files.internal(s + "/" + s1));
                }
            });
            data.character = character;
        }

    }

    private float textY = TEXT_Y_START;
    private float defaultTextY = TV_TOP;

    public void update(GameState game) {

        batch.setProjectionMatrix(game.camera.combined);

        textY = TEXT_Y_START;
        defaultTextY = TV_TOP;


        for (Entity e: entities) {
            if (e.isVisible) {

                batch.begin();

                if (e.renderData.character != null) {
                    drawChar(e, e.renderData);
                    drawText(e, e.renderData);
                } else if (e.body != null) {
                        batch.draw(e.img, e.body.getPosition().x - e.width / 2.0f,
                            e.body.getPosition().y - e.height / 2.0f, e.width, e.height);
                } else if (e.center != null) {
                    float r = e.has("rotation") ? e.get("rotation", Float.class) : 0;
                    Matrix4 m = batch.getTransformMatrix();
                    Matrix4 c = m.cpy();
                    m.translate(e.center.x - e.width / 2, e.center.y - e.height / 2, 0);
                    m.rotate(0,0,1, -r);
                    batch.setTransformMatrix(m);
                    batch.draw(e.img, 0,  0, e.width, e.height);
                    batch.setTransformMatrix(c);
                }

                batch.end();
            }
        }
        List<Message> forRemoval = new ArrayList<Message>();
        batch.begin();
        for (Message m: messages
             ) {
            drawMessage(m);
            if (m.framesLeft <= 0){
                forRemoval.add(m);
            }
        }
        batch.end();
        for (Message m: forRemoval
             ) {
            messages.remove(m);
        }

    }


    public void drawMessage(Message message){
        float x;
        float y;
        if (message.defaultPosition){
            y = defaultTextY;
            if (y < TV_BOTTOM){
                return;
            }
            x = TV_LEFT;
            defaultTextY -= TEXT_Y_SHIFT*1.1f;
        }
        else {
            x = message.pos.x;
            y = message.pos.y;
        }
        font.draw(batch, message.s, x, y);
        message.framesLeft--;
        if (!message.defaultPosition) {
            message.pos.y += 0.1;
            message.pos.x -= 0.2;
        }
    }

    private static final float TEXT_X = 32f;
    private static final float TEXT_Y_START = 87f;
    private static final float TEXT_Y_SHIFT = 1.5f;



    /**
     * Draws text associated to the entity
     * @param e
     * @param d
     */
    public void drawText(Entity e, RenderData d){
        if (e.has("buttons")){
            float x = e.body.getPosition().x;
            float y = e.body.getPosition().y;
            String buttons = e.get("buttons", String.class);
            String name = e.name + " (" + e.scoringData.totalScore +")";
            //TODO update the position of text
            //font.draw(batch, name.toUpperCase(), 11.0f, y);
            //font.draw(batch,"Hello World!", 11, y);
            //System.out.println(name);
            float i = 0.0f;
            Color tint = e.get("color", Color.class);
            font.setColor(tint);
            font.draw(batch, name, TEXT_X, textY);
            textY -= TEXT_Y_SHIFT;
            font.setColor(Color.WHITE);
//            for (char c : name.toCharArray()) {
//                font.draw(batch,c+"", 11+i, y);
//                i+=2f;
//            }
        }
    }

    /**
     * Draw character
     * @param e
     * @param d
     */
    public void drawChar(Entity e, RenderData d) {
        Matrix4 m = batch.getTransformMatrix();
        Matrix4 mcopy = m.cpy();
        m.translate(e.body.getPosition().x, e. body.getPosition().y, 0);

        m.scl(0.8f, 0.8f, 1f);
        m.rotateRad(0,0,1, e.body.getAngle() - (float)Math.PI/2.0f);

        boolean animationDone = d.incrementFrame();
        if (animationDone) {
            game.addEvent(new Events.AnimationOverEvent(e, d.getCurrentAnimation().getName()));
        }
        BodyNode root = d.getNodeToDraw();
        Color tint = Color.WHITE;
        if (e.has("color")) {
            tint = e.get("color", Color.class);
        }

        drawNode(root, m, tint);

        batch.setTransformMatrix(mcopy);
    }

    /**
     * Draws parts of the character
     * @param bodyNode
     * @param m
     */
    private void drawNode(BodyNode bodyNode, Matrix4 m, Color diperTint) {
        m.rotateRad(0.0f, 0.0f, 1.0f, -bodyNode.getAngle());
        m.translate(bodyNode.getDistance() / game.tileWidth, 0, 0);


        for (int i = 0; i < bodyNode.getChildCount(); i++) {
            if (((BodyNode)bodyNode.getChild(i)).getzAxis() < bodyNode.getzAxis())
            {
                drawNode((BodyNode) bodyNode.getChild(i), m, diperTint);
            }
        }

        Texture image = (Texture)bodyNode.getImage();
        if (image != null) {

            float adjust = 0;//(float)Math.PI/4;
            float angle = bodyNode.getImageAngle() +adjust;
            m.rotateRad(0, 0, 1, -angle);
            batch.setTransformMatrix(m);
            boolean diper = bodyNode.getName().equals("diper");
            if (diper) {
                batch.setColor(diperTint);
            }
            batch.draw(image,
                (float)image.getWidth() / game.tileWidth / -2.0f,
                (float)image.getHeight() / game.tileWidth / -2.0f,
                ((float)image.getWidth()) / game.tileWidth,
                ((float)image.getHeight()) / game.tileWidth);
            if (diper) {
                batch.setColor(Color.WHITE);
            }
            m.rotateRad(0, 0, 1, angle);
        }

        for (int i = 0; i < bodyNode.getChildCount(); i++) {
            if (((BodyNode)bodyNode.getChild(i)).getzAxis() >= bodyNode.getzAxis())
            {
                drawNode((BodyNode) bodyNode.getChild(i), m, diperTint);
            }
        }
        m.translate(-bodyNode.getDistance() / game.tileWidth, 0, 0);
        m.rotateRad(0, 0, 1, bodyNode.getAngle());

    }

    public void dispose() {
        batch.dispose();
        for (Entity e : entities) {
            e.img.dispose();
        }
    }

    public void removeEntity(Entity e) {
        entities.remove(e);
    }
}

package no.gj.hg;


// This class is just for easy reference to the strings of component properties, so we can add them in Editor.
public class Components
{
    // Camera component
    public static final String CAMERA = "camera";

    // Player component
    public static final String IS_BORN = "isBorn";
    public static final String CHARACTER = "character";
    public static final String INPUT = "input";
    public static final String BUTTONS = "buttons";
    public static final String PLAYER_NAME = "name";

    // Item component
    public static final String ITEM_CONTROLLER = "item.controller";
    public static final String ITEM_CONTROLLER_NUMBER = "item.controller_number";

    // Light component
    public static final String LIGHT_COLOR = "light.color";
    public static final String LIGHT_DISTANCE = "light.distance";
    public static final String LIGHT_TYPE = "light.type";
    public static final String LIGHT_DIRECTION_DEG = "light.direction_degree";
    public static final String LIGHT_CONE_DEG = "light.cone_degree";
    public static final String LIGHT_STATIC = "light.static_only";
    public static final String LIGHT_CONE = "cone";
    public static final String LIGHT_POINT = "point";

    // Physics component
    public static final String PHYS_TYPE = "phys.type";
    public static final String POS_X = "pos.x";
    public static final String POS_Y = "pos.y";
    public static final String CIRCLE = "circle";
    public static final String POLYGON = "polygon";
    public static final String PHYS_STATIC = "phys.static";

    // Render component
    public static final String RENDER_WIDTH = "render.width";
    public static final String RENDER_HEIGHT = "render.height";
    public static final String RENDER_TEXTURE = "render.texture";

}










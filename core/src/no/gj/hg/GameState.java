package no.gj.hg;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import no.gj.hg.systems.CSystem;
import no.gj.hg.systems.EventSystem;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GameState {
    public OrthographicCamera camera;
    public List<Entity> entityList = new LinkedList<Entity>();
    public List<CSystem> systems = new ArrayList<CSystem>();

    public List<EventSystem> eventSystems = new ArrayList<EventSystem>();

    //the list of things to be removed
    public List<Entity> toRemove = new ArrayList<Entity>();
    //the list of new stuff to be added
    public List<Entity> toAdd = new ArrayList<Entity>();

    public LinkedList<Entity> unbornBabies = new LinkedList<Entity>();


    public List<Event> events = new ArrayList<Event>();
    public World world ;
    public int tileWidth;

    public void addEvent(Event event) {
        events.add(event);
    }


}

README
============

## Building

    gradlew build

## Running

    gradlew run

## Building release

	gradlew desktop:dist


The result is located in:

	desktop/build/libs/desktop-1.0.jar
	core/build/libs/core-1.0.jar

### Other files needed.

Also need to copy the contents of core/assets



Then it should be possible to run the application:

    java -jar desktop-1.0.jar





